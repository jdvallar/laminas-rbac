<?php


namespace Vallarj\Laminas\Rbac\Subscriber;


use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Vallarj\Laminas\Rbac\Entity\AbstractUser;

class UserIndexSubscriber implements EventSubscriber
{
    /**
     * @inheritDoc
     */
    public function getSubscribedEvents()
    {
        return [
            Events::loadClassMetadata
        ];
    }

    /**
     * Subscriber on loadClassMetadata
     *
     * @param LoadClassMetadataEventArgs $eventArgs
     * @return void
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $classMetadata = $eventArgs->getClassMetadata();
        if (is_a($classMetadata->getName(), AbstractUser::class, true)) {
            $classMetadata->table['indexes'][] = [
                'columns' => [
                    'name_index',
                    'id'
                ],
            ];
        }
    }
}
