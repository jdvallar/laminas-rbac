<?php


namespace Vallarj\Laminas\Rbac;


class Module
{
    /**
     * Returns the module configuration
     *
     * @return array
     */
    public function getConfig(): array
    {
        return (new ConfigProvider())->getModuleConfiguration();
    }
}
