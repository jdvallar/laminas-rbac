<?php


namespace Vallarj\Laminas\Rbac\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="_rbac_permission")
 */
class Permission
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="string", length=255)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * Permission constructor.
     *
     * @param string $id
     * @param string $description
     */
    public function __construct(string $id, string $description)
    {
        $this->id = $id;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Permission
     */
    public function setDescription(string $description): Permission
    {
        $this->description = $description;
        return $this;
    }
}
