<?php


namespace Vallarj\Laminas\Rbac\Entity;


use Vallarj\Laminas\Rbac\Utilities\Password\PasswordInterface;

interface RbacUser
{
    /**
     * Returns the user ID
     *
     * @return string
     */
    public function getId(): string;

    /**
     * Returns the user's username
     *
     * @return string
     */
    public function getUsername(): string;

    /**
     * Returns true if user is active
     *
     * @return bool
     */
    public function isActive(): bool;

    /**
     * Returns the user roles
     *
     * @return Role[]
     */
    public function getRoles(): array;

    /**
     * Returns the name sorting index
     *
     * @return string
     */
    public function getNameIndex(): string;

    /**
     * Returns the user permissions
     *
     * @return array
     */
    public function getPermissions(): array;

    /**
     * Returns true if user has permission with specified permission ID
     *
     * @param string $permissionId
     * @return bool
     */
    public function hasPermission(string $permissionId): bool;

    /**
     * Returns true if specified password matches the current password
     *
     * @param string $password
     * @param PasswordInterface $passwordService
     * @return bool
     */
    public function authenticate(string $password, PasswordInterface $passwordService): bool;
}
