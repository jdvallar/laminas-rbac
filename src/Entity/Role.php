<?php


namespace Vallarj\Laminas\Rbac\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="_rbac_role")
 */
class Role
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="string", length=255)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="`default`", type="boolean")
     */
    private $default;

    /**
     * @var boolean
     *
     * @ORM\Column(name="`internal`", type="boolean")
     */
    private $internal;

    /**
     * @var ArrayCollection|Permission[]
     *
     * @ORM\ManyToMany(targetEntity="Permission", indexBy="id")
     * @ORM\JoinTable(name="_rbac_role_permission",
     *     joinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="permission_id", referencedColumnName="id", onDelete="CASCADE")}
     *     )
     */
    private $permissions;

    /**
     * @var RoleType|null
     *
     * @ORM\ManyToOne(targetEntity="RoleType")
     * @ORM\JoinColumn(name="role_type_id", referencedColumnName="id")
     */
    private $roleType;

    /**
     * Role constructor.
     *
     * @param string $id
     * @param string $name
     * @param RoleType|null $roleType
     * @param bool $default
     * @param bool $internal
     */
    public function __construct(string $id, string $name, ?RoleType $roleType, bool $default, bool $internal)
    {
        $this->id = $id;
        $this->name = $name;
        $this->roleType = $roleType;
        $this->default = $default;
        $this->internal = $internal;

        $this->permissions = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Role
     */
    public function setName(string $name): Role
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return RoleType|null
     */
    public function getRoleType(): ?RoleType
    {
        return $this->roleType;
    }

    /**
     * @param RoleType|null $roleType
     * @return void
     */
    public function internalSetRoleType(?RoleType $roleType): void
    {
        $this->roleType = $roleType;
    }

    /**
     * @return bool
     */
    public function getDefault(): bool
    {
        return $this->default;
    }

    /**
     * @param Permission[] $permissions
     * @return static
     */
    public function setPermissions(array $permissions): Role
    {
        $this->permissions->clear();
        foreach ($permissions as $permission) {
            $this->permissions->add($permission);
        }

        return $this;
    }

    /**
     * @return Permission[]
     */
    public function getPermissions(): array
    {
        return $this->permissions->toArray();
    }

    /**
     * @param string $permissionId
     * @return bool
     */
    public function hasPermission(string $permissionId): bool
    {
        if (substr($permissionId, -2) === ".*") {
            // Wildcard
            $permSet = explode('.', $permissionId);
            $length = count($permSet) - 1;
            $wildcardPerm = array_slice($permSet, 0, $length);
            foreach ($this->permissions as $permission) {
                if ($wildcardPerm === array_slice(explode('.', $permission->getId()), 0, $length)) {
                    return true;
                }
            }

            return false;
        } else {
            return isset($this->permissions[$permissionId]);
        }
    }

    /**
     * @return bool
     */
    public function isInternal(): bool
    {
        return $this->internal;
    }
}
