<?php


namespace Vallarj\Laminas\Rbac\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Vallarj\Laminas\Rbac\Exception\InvalidTemporaryPasswordException;
use Vallarj\Laminas\Rbac\Utilities\Password\PasswordInterface;
use Ramsey\Uuid\Uuid;

abstract class AbstractUser implements RbacUser
{
    /**
     * @var Uuid
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="uuid_binary_ordered_time", unique=true)
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidOrderedTimeGenerator")
     */
    protected $id;

    /**
     * @var ArrayCollection|Role[]
     *
     * @ORM\ManyToMany(targetEntity="Vallarj\Laminas\Rbac\Entity\Role")
     * @ORM\JoinTable(name="_rbac_user_role",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id", onDelete="CASCADE")}
     *     )
     */
    protected $roles;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", length=2000)
     */
    protected $password;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=150)
     */
    protected $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="middle_name", type="string", length=150, nullable=true)
     */
    protected $middleName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=150)
     */
    protected $lastName;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    protected $active;

    /**
     * @var null|string
     *
     * @ORM\Column(name="temporary_password", length=10, nullable=true)
     */
    protected $temporaryPassword;

    /**
     * @var bool
     *
     * @ORM\Column(name="uses_temporary_password", type="boolean")
     */
    protected $usesTemporaryPassword;

    /**
     * @var bool
     *
     * @ORM\Column(name="internal", type="boolean")
     */
    protected $internal;

    /**
     * @var string
     *
     * @ORM\Column(name="name_index", type="string", length=450)
     */
    protected $nameIndex;

    /** @var array */
    private $checkedPermissions = [];

    /**
     * AbstractUser constructor.
     *
     * @param string $username
     * @param string $password
     * @param string $firstName
     * @param string $lastName
     * @param bool $isTemporaryPassword
     * @param PasswordInterface $passwordService
     * @throws InvalidTemporaryPasswordException
     */
    public function __construct(
        string $username,
        string $password,
        string $firstName,
        string $lastName,
        bool $isTemporaryPassword,
        PasswordInterface $passwordService
    ) {
        $this->active = true;
        $this->internal = false;
        $this->username = $username;
        $this->password = $passwordService->hash($password);
        $this->firstName = $firstName;
        $this->lastName = $lastName;

        if ($isTemporaryPassword) {
            $this->setTemporaryPassword($password, $passwordService);
        } else {
            $this->changePassword($password, $passwordService);
        }

        $this->roles = new ArrayCollection();
    }

    /**
     * @inheritDoc
     */
    public function getId(): string
    {
        return $this->id->toString();
    }

    /**
     * @inheritDoc
     */
    public function getRoles(): array
    {
        return $this->roles->toArray();
    }

    /**
     * @param Role[] $roles
     * @return static
     */
    public function setRoles(array $roles): AbstractUser
    {
        $this->roles->clear();
        foreach ($roles as $role) {
            $this->roles->add($role);
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getPermissions(): array
    {
        $permissions = [];
        foreach ($this->roles as $role) {
            foreach ($role->getPermissions() as $rolePermission) {
                $permissions[$rolePermission->getId()] = $rolePermission;
            }
        }

        return $permissions;
    }

    /**
     * @inheritDoc
     */
    public function hasPermission(string $permissionId): bool
    {
        if (!array_key_exists($permissionId, $this->checkedPermissions)) {
            $this->checkedPermissions[$permissionId] = false;
            foreach ($this->roles as $role) {
                if ($role->hasPermission($permissionId)) {
                    $this->checkedPermissions[$permissionId] = true;
                }
            }
        }

        return $this->checkedPermissions[$permissionId];
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $password
     * @param PasswordInterface $passwordService
     * @return AbstractUser
     */
    public function changePassword(string $password, PasswordInterface $passwordService): AbstractUser
    {
        $this->usesTemporaryPassword = false;
        $this->password = $passwordService->hash($password);
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return AbstractUser
     */
    public function setFirstName(string $firstName): AbstractUser
    {
        $this->firstName = $firstName;
        $this->recomputeNameIndex();
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    /**
     * @param string|null $middleName
     * @return AbstractUser
     */
    public function setMiddleName(?string $middleName): AbstractUser
    {
        $this->middleName = $middleName;
        $this->recomputeNameIndex();
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return AbstractUser
     */
    public function setLastName(string $lastName): AbstractUser
    {
        $this->lastName = $lastName;
        $this->recomputeNameIndex();
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return AbstractUser
     */
    public function setActive(bool $active): AbstractUser
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryPassword(): ?string
    {
        return $this->temporaryPassword;
    }

    /**
     * @param string|null $temporaryPassword
     * @param PasswordInterface $passwordService
     * @return AbstractUser
     * @throws InvalidTemporaryPasswordException
     */
    public function setTemporaryPassword(string $temporaryPassword, PasswordInterface $passwordService): AbstractUser
    {
        if (strlen($temporaryPassword) > 10) {
            throw new InvalidTemporaryPasswordException("Temporary password length must not be greater than 10");
        }

        $this->usesTemporaryPassword = true;
        $this->temporaryPassword = $temporaryPassword;
        $this->password = $passwordService->hash($temporaryPassword);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function authenticate(string $password, PasswordInterface $passwordService): bool
    {
        return $passwordService->verify($password, $this->password);
    }

    /**
     * @return bool
     */
    public function getUsesTemporaryPassword(): bool
    {
        return $this->usesTemporaryPassword;
    }

    /**
     * @return string
     */
    public function getNameIndex(): string
    {
        return $this->nameIndex;
    }

    /**
     * Recomputes the name index
     *
     * @return void
     */
    private function recomputeNameIndex(): void
    {
        $this->nameIndex = strtoupper($this->lastName) . "_" .
            strtoupper($this->firstName) . "_" . strtoupper($this->middleName ?? "");
    }
}
