<?php


namespace Vallarj\Laminas\Rbac\Factory\Repository;


use Doctrine\ORM\EntityManager;
use Vallarj\Laminas\Rbac\Repository\PermissionRepository;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PermissionRepositoryFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new PermissionRepository($container->get(EntityManager::class));
    }
}
