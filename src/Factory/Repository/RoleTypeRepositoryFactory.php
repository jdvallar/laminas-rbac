<?php

declare(strict_types=1);

namespace Vallarj\Laminas\Rbac\Factory\Repository;


use Doctrine\ORM\EntityManager;
use Vallarj\Laminas\Rbac\Repository\RoleTypeRepository;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class RoleTypeRepositoryFactory implements FactoryInterface
{
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new RoleTypeRepository($container->get(EntityManager::class));
    }
}
