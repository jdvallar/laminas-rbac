<?php


namespace Vallarj\Laminas\Rbac\Factory\Repository;


use Doctrine\ORM\EntityManager;
use Vallarj\Laminas\Rbac\Repository\RoleRepository;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class RoleRepositoryFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new RoleRepository($container->get(EntityManager::class));
    }
}
