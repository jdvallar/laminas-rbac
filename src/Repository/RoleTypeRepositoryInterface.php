<?php

declare(strict_types=1);

namespace Vallarj\Laminas\Rbac\Repository;


use Vallarj\Laminas\Rbac\Entity\RoleType;

interface RoleTypeRepositoryInterface
{
    /**
     * Fetches all role types
     *
     * @return RoleType[]
     */
    public function findAllRoleTypes(): array;

    /**
     * Gets a role type by ID
     *
     * @param string $id
     * @return RoleType
     */
    public function getRoleType(string $id): RoleType;
}
