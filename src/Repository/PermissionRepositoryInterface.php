<?php


namespace Vallarj\Laminas\Rbac\Repository;


use Vallarj\Laminas\Rbac\Entity\Permission;

interface PermissionRepositoryInterface
{
    /**
     * Fetches all permissions
     *
     * @return Permission[]
     */
    public function findAllPermissions(): array;

    /**
     * Gets a permission by id
     *
     * @param string $id
     * @return Permission
     */
    public function getPermission(string $id): Permission;
}
