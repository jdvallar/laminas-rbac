<?php


namespace Vallarj\Laminas\Rbac\Repository;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Vallarj\Laminas\Rbac\Entity\Role;
use Vallarj\Laminas\Rbac\Exception\EntityNotFoundException;

class RoleRepository implements RoleRepositoryInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * RoleRepository constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritDoc
     */
    public function findAllRoles(): array
    {
        $qb = $this->entityManager->createQueryBuilder();

        return $qb->select('r')
            ->from(Role::class, 'r')
            ->where('r.internal = ?1')
            ->setParameter(1, false)
            ->getQuery()
            ->getResult();
    }

    /**
     * @inheritDoc
     */
    public function findAllRolesByRoleTypes(array $roleTypes): array
    {
        $roleTypeIds = [];
        foreach ($roleTypes as $roleType) {
            if (is_string($roleType)) {
                $roleTypeIds[] = $roleType;
            }
        }

        if (empty($roleTypeIds)) {
            return [];
        }

        $qb = $this->entityManager->createQueryBuilder();

        return $qb->select('r')
            ->from(Role::class, 'r')
            ->where('r.internal = ?1')
            ->andWhere($qb->expr()->orX(
                $qb->expr()->isNull('r.roleType'),
                $qb->expr()->in('r.roleType', '?2')
            ))
            ->setParameter(1, false)
            ->setParameter(2, $roleTypeIds)
            ->getQuery()
            ->getResult();
    }

    /**
     * @inheritDoc
     * @throws NonUniqueResultException
     * @throws EntityNotFoundException
     */
    public function getRole(string $id): Role
    {
        $qb = $this->entityManager->createQueryBuilder();

        $role = $qb->select('r')
            ->from(Role::class, 'r')
            ->where('r.id = ?1 AND r.internal = ?2')
            ->setParameter(1, $id)
            ->setParameter(2, false)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$role) {
            throw new EntityNotFoundException("Role with ID: $id not found.");
        }

        return $role;
    }
}
