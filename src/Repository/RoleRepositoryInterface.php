<?php


namespace Vallarj\Laminas\Rbac\Repository;


use Vallarj\Laminas\Rbac\Entity\Role;

interface RoleRepositoryInterface
{
    /**
     * Fetches all roles
     *
     * @return Role[]
     */
    public function findAllRoles(): array;

    /**
     * Fetches all roles by role type ID
     *
     * @param string[] $roleTypes
     * @return Role[]
     */
    public function findAllRolesByRoleTypes(array $roleTypes): array;

    /**
     * Gets a role by id
     *
     * @param string $id
     * @return Role
     */
    public function getRole(string $id): Role;
}
