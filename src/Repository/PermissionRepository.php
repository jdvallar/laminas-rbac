<?php


namespace Vallarj\Laminas\Rbac\Repository;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Vallarj\Laminas\Rbac\Entity\Permission;
use Vallarj\Laminas\Rbac\Exception\EntityNotFoundException;

class PermissionRepository implements PermissionRepositoryInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * PermissionRepository constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritDoc
     */
    public function findAllPermissions(): array
    {
        $qb = $this->entityManager->createQueryBuilder();

        return $qb->select('p')
            ->from(Permission::class, 'p')
            ->getQuery()
            ->getResult();
    }

    /**
     * @inheritDoc
     * @throws NonUniqueResultException
     * @throws EntityNotFoundException
     */
    public function getPermission(string $id): Permission
    {
        $qb = $this->entityManager->createQueryBuilder();

        $permission = $qb->select('p')
            ->from(Permission::class, 'p')
            ->where('p.id = ?1')
            ->setParameter(1, $id)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$permission) {
            throw new EntityNotFoundException("Permission with ID: ${id} not found.");
        }

        return $permission;
    }
}
