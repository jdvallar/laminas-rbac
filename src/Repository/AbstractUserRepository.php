<?php


namespace Vallarj\Laminas\Rbac\Repository;


use Doctrine\DBAL\Types\ConversionException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Vallarj\Laminas\Rbac\Entity\RbacUser;
use Vallarj\Laminas\Rbac\Entity\Role;
use Vallarj\Laminas\Rbac\Exception\RbacUserNotFoundException;

abstract class AbstractUserRepository implements RbacUserRepositoryInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * Returns the FQCN of the user class
     *
     * @return string
     */
    abstract public function getUserClass(): string;

    /**
     * AbstractUserRepository constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Returns the entity manager
     *
     * @return EntityManagerInterface
     */
    final public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @inheritDoc
     * @throws NonUniqueResultException
     * @throws RbacUserNotFoundException
     */
    final public function getBaseUser(string $id): RbacUser
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $user = $queryBuilder->select('ru')
            ->from($this->getUserClass(), 'ru')
            ->where('ru.id = ?1')
            ->setParameter(1, $id, 'uuid_binary_ordered_time')
            ->getQuery()
            ->getOneOrNullResult();

        if (!$user) {
            throw new RbacUserNotFoundException("Rbac User with ID: ${id} not found.");
        }

        return $user;
    }

    /**
     * @inheritDoc
     * @throws NonUniqueResultException
     * @throws RbacUserNotFoundException
     */
    public function getBaseUserByUsername(string $username): RbacUser
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $user = $queryBuilder->select('ru')
            ->from($this->getUserClass(), 'ru')
            ->where('ru.username = ?1')
            ->setParameter(1, $username)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$user) {
            throw new RbacUserNotFoundException("Rbac User with username: ${username} not found.");
        }

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function findAllUsers(): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        return $qb->select('u')
            ->from($this->getUserClass(), 'u')
            ->where('u.internal = ?1')
            ->setParameter(1, false)
            ->getQuery()
            ->getResult();
    }

    /**
     * @inheritDoc
     */
    public function findAllUsedRoles(): array
    {
        $qb = $this->entityManager->createQueryBuilder();

        return $qb->select('r')
            ->from($this->getUserClass(), 'u')
            ->join('u.roles', 'ur')
            ->join(Role::class, 'r', 'WITH', 'r.id = ur.id')
            ->groupBy('ur.id')
            ->getQuery()
            ->getResult();
    }

    /**
     * @inheritDoc
     */
    public function findUsers(int $pageCount, ?string $searchQuery, ?string $lastId): array
    {
        // Tokenize search query
        $searchQuery = trim($searchQuery);
        $tokens = [];
        if ($searchQuery) {
            $tokens = array_map(function ($w) {
                return '%' . addcslashes($w, '\\%_') . '%';
            }, array_filter(preg_split("/\s+/", $searchQuery), function ($w) {
                return $w !== "";
            }));
        }

        $results = [];
        if ($lastId) {
            try {
                $lastUser = $this->getUser($lastId);
            } catch (RbacUserNotFoundException $e) {
                return [];
            }

            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select('u')
                ->from($this->getUserClass(), 'u')
                ->where('u.internal = ?1')
                ->andWhere('u.nameIndex = ?2')
                ->andWhere('u.id > ?3')
                ->setParameter(1, false)
                ->setParameter(2, $lastUser->getNameIndex())
                ->setParameter(3, $lastUser->getId(), 'uuid_binary_ordered_time');

            if (!empty($tokens)) {
                $this->addSearchParameters($qb, $tokens, 4);
            }

            $qb->orderBy('u.nameIndex', 'ASC')
                ->addOrderBy('u.id', 'ASC')
                ->setMaxResults($pageCount);

            $results = $qb->getQuery()->getResult();
        }

        if ($pageCount - count($results) > 0) {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select('u')
                ->from($this->getUserClass(), 'u')
                ->where('u.internal = ?1')
                ->setParameter(1, false);

            $i = 2;
            if (isset($lastUser)) {
                $qb->andWhere('u.nameIndex > ?2')
                    ->setParameter(2, $lastUser->getNameIndex());
                $i++;
            }

            if (!empty($tokens)) {
                $this->addSearchParameters($qb, $tokens, $i);
            }

            $qb->orderBy('u.nameIndex', 'ASC')
                ->addOrderBy('u.id', 'ASC')
                ->setMaxResults($pageCount - count($results));

            $remaining = $qb->getQuery()->getResult();
            foreach ($remaining as $item) {
                $results[] = $item;
            }
        }

        return $results;
    }

    /**
     * Adds search parameters to the specified query builder
     *
     * @param QueryBuilder $qb
     * @param string[] $tokens
     * @param int $paramStart
     * @return void
     */
    private function addSearchParameters(QueryBuilder $qb, array $tokens, int $paramStart): void
    {
        foreach ($tokens as $token) {
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->like('u.firstName', "?$paramStart"),
                $qb->expr()->like('u.middleName', "?$paramStart"),
                $qb->expr()->like('u.lastName', "?$paramStart"),
                $qb->expr()->like('u.username', "?$paramStart")
            ));

            $qb->setParameter($paramStart, $token);
            $paramStart++;
        }
    }

    /**
     * @inheritDoc
     * @throws NonUniqueResultException
     * @throws RbacUserNotFoundException
     */
    public function getUser(string $id): RbacUser
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        try {
            $user = $queryBuilder->select('u')
                ->from($this->getUserClass(), 'u')
                ->where('u.id = ?1')
                ->setParameter(1, $id, 'uuid_binary_ordered_time')
                ->getQuery()
                ->getOneOrNullResult();
        } catch (ConversionException $exception) {
            $user = null;
        }

        if (!$user) {
            throw new RbacUserNotFoundException("Rbac User with ID: ${id} not found.");
        }

        return $user;
    }
}
