<?php


namespace Vallarj\Laminas\Rbac\Repository;


use Vallarj\Laminas\Rbac\Entity\RbacUser;
use Vallarj\Laminas\Rbac\Entity\Role;

interface RbacUserRepositoryInterface
{
    /**
     * Returns the base user entity with the specified user ID
     *
     * @param string $id
     * @return RbacUser
     */
    public function getBaseUser(string $id): RbacUser;

    /**
     * Returns the base user entity with the specified username
     *
     * @param string $username
     * @return RbacUser
     */
    public function getBaseUserByUsername(string $username): RbacUser;

    /**
     * Fetches all non-internal users
     *
     * @return RbacUser[]
     */
    public function findAllUsers(): array;

    /**
     * Fetches all used roles
     *
     * @return Role[]
     */
    public function findAllUsedRoles(): array;

    /**
     * Returns ObligationRequests according to specified parameters, sorted by name
     *
     * @param int $pageCount
     * @param string|null $searchQuery
     * @param string|null $lastId
     * @return array
     */
    public function findUsers(int $pageCount, ?string $searchQuery, ?string $lastId): array;

    /**
     * Gets a non-internal user by ID
     *
     * @param string $id
     * @return RbacUser
     */
    public function getUser(string $id): RbacUser;
}
