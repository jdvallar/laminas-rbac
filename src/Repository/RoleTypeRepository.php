<?php

declare(strict_types=1);

namespace Vallarj\Laminas\Rbac\Repository;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Vallarj\Laminas\Rbac\Entity\RoleType;
use Vallarj\Laminas\Rbac\Exception\EntityNotFoundException;

class RoleTypeRepository implements RoleTypeRepositoryInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * RoleTypeRepository constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritDoc
     */
    public function findAllRoleTypes(): array
    {
        $qb = $this->entityManager->createQueryBuilder();

        return $qb->select('rt')
            ->from(RoleType::class, 'rt')
            ->getQuery()
            ->getResult();
    }

    /**
     * @inheritDoc
     * @throws NonUniqueResultException
     * @throws EntityNotFoundException
     */
    public function getRoleType(string $id): RoleType
    {
        $qb = $this->entityManager->createQueryBuilder();

        $roleType = $qb->select('rt')
            ->from(RoleType::class, 'rt')
            ->where('rt.id = ?1')
            ->setParameter(1, $id)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$roleType) {
            throw new EntityNotFoundException("Role type with ID: ${id} not found.");
        }

        return $roleType;
    }
}
