<?php


namespace Vallarj\Laminas\Rbac;


use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Vallarj\Laminas\Rbac\Subscriber\UserIndexSubscriber;
use Laminas\ServiceManager\Factory\InvokableFactory;

class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'doctrine' => $this->getDoctrineConfiguration(),
        ];
    }

    /**
     * Returns the module configuration for laminas-mvc projects
     *
     * @return array
     */
    public function getModuleConfiguration(): array
    {
        return [
            'service_manager' => $this->getDependencies(),
            'doctrine' => $this->getDoctrineConfiguration()
        ];
    }

    /**
     * Returns the module dependencies
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            'factories' => [
                // Utilities
                Utilities\Password\Bcrypt::class => InvokableFactory::class,

                // Repositories
                Repository\PermissionRepository::class => Factory\Repository\PermissionRepositoryFactory::class,
                Repository\RoleRepository::class => Factory\Repository\RoleRepositoryFactory::class,
                Repository\RoleTypeRepository::class => Factory\Repository\RoleTypeRepositoryFactory::class,
            ],
        ];
    }

    /**
     * Returns Doctrine configuration
     *
     * @return array
     */
    public function getDoctrineConfiguration(): array
    {
        return [
            'driver' => [
                __NAMESPACE__ . '_driver' => [
                    'class' => AnnotationDriver::class,
                    'cache' => 'array',
                    'paths' => [
                        __DIR__ . '/Entity',
                    ],
                ],
                'orm_default' => [
                    'drivers' => [
                        __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                    ],
                ],
            ],
            'eventmanager' => [
                'orm_default' => [
                    'subscribers' => [
                        UserIndexSubscriber::class
                    ]
                ]
            ]
        ];
    }
}
