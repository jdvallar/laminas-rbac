<?php

namespace Vallarj\Laminas\Rbac\Utilities\Password;


class Bcrypt implements PasswordInterface
{
    /**
     * @inheritdoc
     */
    public function hash(string $password): string
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    /**
     * @inheritdoc
     */
    public function verify(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }
}
