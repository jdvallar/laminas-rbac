<?php

namespace Vallarj\Laminas\Rbac\Utilities\Password;


interface PasswordInterface
{
    /**
     * Hash a password
     *
     * @param string $password
     * @return string
     */
    public function hash(string $password): string;

    /**
     * Verify that a password matches a hash
     *
     * @param string $password
     * @param string $hash
     * @return bool
     */
    public function verify(string $password, string $hash): bool;
}
