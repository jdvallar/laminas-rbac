<?php


namespace Vallarj\Laminas\Rbac\Exception;


class EntityNotFoundException extends Exception
{
}
