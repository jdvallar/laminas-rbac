<?php


namespace Vallarj\Laminas\Rbac\Exception;


class RbacUserNotFoundException extends Exception
{
}
