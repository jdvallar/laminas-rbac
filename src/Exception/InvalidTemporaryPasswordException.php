<?php


namespace Vallarj\Laminas\Rbac\Exception;


class InvalidTemporaryPasswordException extends Exception
{
}
